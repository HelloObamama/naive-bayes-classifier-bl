#!/bin/bash
cd /home/bingweiliu/NBClassifier
rm -rf NB_class
rm -f NBClassifier.jar
mkdir NB_class
javac -cp `hadoop classpath` -d NB_class *.java 

jar -cvf NBClassifier.jar -C NB_class/ .

for i in `seq 1 10`
do
# prepare training data
 hdfs dfs -rm review-in/*
 hdfs dfs -rm test/*
 hdfs dfs -copyFromLocal dataset/trial$i/train/*.csv review-in/
 hdfs dfs -copyFromLocal dataset/trial$i/test/*.csv test/
hadoop jar NBClassifier.jar com.ift.hadoop.NBController -D train=review-in -D test=test -D output=reviewout -D reducer=20 -D result=result$i 
#retrieve model
rm -f part-r*
hdfs dfs -copyToLocal model/part-r* .
cat part-r* > dataset/trial$i/model.csv
scp dataset/trial$i/model.csv 192.168.1.2:/home/bingweiliu/Desktop/trial$i
# retrieve output
rm -f part-r*
hdfs dfs -copyToLocal testprepare/part-r* .
cat part-r* > dataset/trial$i/testprepare.csv
scp dataset/trial$i/testprepare.csv 192.168.1.2:/home/bingweiliu/Desktop/trial$i
#
rm -f part-r*
hdfs dfs -copyToLocal reviewout/part-r* .
cat part-r* > dataset/trial$i/reviewout.csv
scp dataset/trial$i/reviewout.csv 192.168.1.2:/home/bingweiliu/Desktop/trial$i
done
rm -f part-r*
rm -f result*
hdfs dfs -copyToLocal result/* .
cat result* > dataset/result.csv
scp dataset/result.csv 192.168.1.2:/home/bingweiliu/Desktop/
